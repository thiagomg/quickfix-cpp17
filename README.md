# quickfix-cpp17

quickfix library fork, compatible with c++17

## Building

Run:

```
./bootstrap
```

and

```
./configure --prefix=$(pwd)/dist
make -j8
```
